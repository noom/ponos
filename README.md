# Ponos

Ponos is a small python3 script that helps you track your work hours using Google Sheets' API.

# Usage

To insert a new row in your spreadsheet:

```bash
./ponos.py
```

To reformate your spreadsheet:

```bash
./ponos.py reformate
```

Currently it only update the cells' background colors.

# Dependencies

This script requires:

- python3
- packages installable with `pip install <package(s)>`:
  * oauth2client
  * gspread
  * gspread_formatting